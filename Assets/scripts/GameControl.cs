﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public GameObject Astroid;
    public Vector3 Randompos;
    public GameObject Player;
    public GameObject Restart;
    public GameObject Exit;
    void Start()
    {
        StartCoroutine(Instantiate());
        
    }
    private void Update()
    {
        if (Player == false)
        {
            Restart.SetActive(true);
            Exit.SetActive(true);
        }
    }
    public void gamerestart()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void gameexit()
    {
        Application.Quit();
    }
    IEnumerator Instantiate ()
    {
        yield return new WaitForSeconds(3);
        while(true)
        {
            for (int i = 0; i < 12; i++)
            {
                Vector3 vec = new Vector3(Random.Range(-Randompos.x, Randompos.x), 0, (Randompos.z));
                Instantiate(Astroid, vec, Quaternion.identity);
                yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(2);
        }
    }

}
