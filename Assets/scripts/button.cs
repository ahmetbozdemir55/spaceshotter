﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class button : MonoBehaviour
{
    public GameObject lazer;
    public Transform lazertransform;
    AudioSource sound;

    private void OnMouseUp()
    {
        sound = GetComponent<AudioSource>();
        Instantiate(lazer, lazertransform.position, Quaternion.identity);
        sound.Play();
    }
    
}
