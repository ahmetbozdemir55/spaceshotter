﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerscript : MonoBehaviour
{
    public Joystick joystick;
 
    
    // Start is called before the first frame update
    void Start()
    {

        joystick = FindObjectOfType<Joystick>();
        Physics.gravity = new Vector3(0, -30, 0);
    }
    
    // Update is called once per frame
    void Update()
    {
        var rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = new Vector3(joystick.Horizontal * 10f, rigidbody.velocity.y, joystick.Vertical * 10f);
        float Xposition = Mathf.Clamp(transform.position.x, -7, 7);
        float Zposition = Mathf.Clamp(transform.position.z, -3, 15);
        transform.position = new Vector3(Xposition, transform.position.y, Zposition);
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="meteor")
        {

            other.gameObject.SetActive(false);

        }
    }
}
