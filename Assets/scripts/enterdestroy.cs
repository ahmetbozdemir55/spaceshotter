﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enterdestroy : MonoBehaviour
{
    public GameObject bang;
    public GameObject playerbang;

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag != "cubelimit")
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
            Instantiate(bang, transform.position, transform.rotation);

        }
        if(col.tag=="Player")
        {
            Instantiate(playerbang, col.transform.position, col.transform.rotation);
        }

    }
}
