﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation : MonoBehaviour
{
    Rigidbody fizik;
    public float hız;
    // Start is called before the first frame update
    void Start()
    {
        fizik = GetComponent<Rigidbody>();
        fizik.angularVelocity = Random.insideUnitSphere*hız;

        fizik.velocity = transform.forward * -4f;
    }

  
}
